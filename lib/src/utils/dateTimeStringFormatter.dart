class DateTimeStringFormatter {
  static final DateTimeStringFormatter instance = DateTimeStringFormatter._internal();

  factory DateTimeStringFormatter() {
    return instance;
  }

  DateTimeStringFormatter._internal();

  String getTimeString(DateTime date) {
    return _getFormatTime(date.hour) + ":" + _getFormatTime(date.minute) + " "  + _getFormatTime(date.day) + "." + _getFormatTime(date.month) + "." + date.year.toString();
  }

  String _getFormatTime(int time) {
    if (time < 10) {
      return "0" + time.toString();
    }

    return time.toString();
  }
}