import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';

class PictureProvider {
  final dbProvider = Firestore.instance;
  final storageRef = FirebaseStorage.instance.ref();

  Future<dynamic> getImageDownloadUrl(String firebaseStorageUrl) {
    final String fileName = firebaseStorageUrl.split(".").first;
    final Directory tempDir = Directory.systemTemp;
    final filePath = '${tempDir.path}/$fileName.txt';

    return File(filePath).existsSync()
        ? _getCachedImageDownloadUrl(filePath)
        : _getImageDownloadUrlFromStorage(firebaseStorageUrl, filePath);
  }

  Future<dynamic> _getCachedImageDownloadUrl(String filePath) {
    return File(filePath).readAsString();
  }

  Future<dynamic> _getImageDownloadUrlFromStorage(
      String firebaseStorageUrl, String filePath) {
    Future<dynamic> imageDownloadUrl;
    imageDownloadUrl = storageRef.child(firebaseStorageUrl).getDownloadURL();

    imageDownloadUrl.then((downloadUrl) {
      File(filePath)
          .create(recursive: true)
          .then((file) => file.writeAsString(downloadUrl));
    });

    return imageDownloadUrl;
  }
}