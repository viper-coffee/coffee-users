import 'package:coffe_users/src/database/DBProvider.dart';
import 'package:coffe_users/src/models/MinimalUserInfo.dart';

class AppUserRepository {
  Future<int> addUserToDB(MinimalUserInfo userToAdd) async {
    final db = await DBProvider.db.database;

    var result = await db.insert(DBProvider.APP_USER_TABLE_NAME, userToAdd.toJson());

    return result;
  }

  Future<int> removeUserFromDB() async {
    final db = await DBProvider.db.database;

    var result = await db.delete(DBProvider.APP_USER_TABLE_NAME);

    return result;
  }

  Future<MinimalUserInfo> getMinimalUserInfoFromDB() async {
    final db = await DBProvider.db.database;

    var result = await db.query(DBProvider.APP_USER_TABLE_NAME);

    MinimalUserInfo appUserToReturn = result.isNotEmpty ? result.map((postInJson) => MinimalUserInfo.fromJson(postInJson)).toList().first : null;

    return appUserToReturn;
  }
}