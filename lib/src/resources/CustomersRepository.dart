import 'package:coffe_users/src/models/Customer.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:coffe_users/src/resources/PictureProvider.dart';
import 'dart:io' as Io;
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:firebase_storage/firebase_storage.dart';

class CustomersRepository {
  static const String USERS_PATH = "users";
  static const String PURCHASES_PATH = "purchases";
  static const String PAYMENTS_PATH = "payments";
  static const String USERS_PICTURES_STORAGE_PATH = "users";

  final PictureProvider _pictureProvider = PictureProvider();

  Stream<DocumentSnapshot> getCustomer(String uId) {
    return Firestore.instance
        .collection(USERS_PATH)
        .document(uId)
        .snapshots();
  }

  Stream<QuerySnapshot> getAllUserPurchases(String uId) {
    return Firestore.instance
        .collection(USERS_PATH)
        .document(uId)
        .collection(PURCHASES_PATH)
        .snapshots();
  }

  Stream<QuerySnapshot> getAllUserPayments(String uId) {
    return Firestore.instance
        .collection(USERS_PATH)
        .document(uId)
        .collection(PAYMENTS_PATH)
        .snapshots();
  }

  Future<Io.File> _getImageFromNetwork(String url) async {
    var cacheManager = DefaultCacheManager();
    Io.File file = await cacheManager.getSingleFile(url);
    return file;
  }

  Future<StorageTaskSnapshot> _saveImageToFirebaseStorage(String url, String name) async {
    final file = await _getImageFromNetwork(url);
    StorageReference reference = FirebaseStorage.instance.ref().child(USERS_PICTURES_STORAGE_PATH + "/" + "$name" + ".jpg");

    return reference.putFile(file).onComplete;
  }

  Future<void> registerUser(String id, String displayName, String photoUrl) async{
    StorageTaskSnapshot storageTaskSnapshot = await _saveImageToFirebaseStorage(photoUrl, id.split("@").first);
    String storagePath = storageTaskSnapshot.ref.path;

    return Firestore.instance.collection(USERS_PATH).document(id).setData({
      Customer.DISPLAY_NAME_FIELD_NAME: displayName,
      Customer.EMAIL_FIELD_NAME: id,
      Customer.PICTURE_URL_FIELD_NAME: storagePath,
    });
  }

  Future<bool> doesCustomerExists(String id) async {
    QuerySnapshot result = await Firestore.instance.collection(USERS_PATH).getDocuments();

    return result.documents.any((customerDocument) => customerDocument.documentID == id);
  }

  Future<dynamic> getImageDownloadUrl(String firebaseStorageUrl) {
    return _pictureProvider.getImageDownloadUrl(firebaseStorageUrl);
  }
}
