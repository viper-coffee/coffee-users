import 'package:cloud_firestore/cloud_firestore.dart';

//TODO make a more generic solution for fetching documents and create Model class from them
class AdminRepository {
  static const USERS_PATH = 'users';
  static const ADMINS_PATH = 'admins';
  static const PURCHASES_PATH = '/purchases';
  final dbProvider = Firestore.instance;

  Stream<QuerySnapshot> fetchUsers() =>
      dbProvider.collection(USERS_PATH).snapshots();

  Stream<QuerySnapshot> fetchRelevantPurchases(String documentID, DateTime fromDate,
      {DateTime toDate}) =>
      dbProvider
          .collection((USERS_PATH + '/$documentID' + PURCHASES_PATH))
          .where('date', isGreaterThanOrEqualTo: fromDate, isLessThan: toDate)
          .snapshots();

  Future<bool> doesAdminExists(String id) async {
    QuerySnapshot result = await Firestore.instance.collection(ADMINS_PATH).getDocuments();

    return result.documents.any((adminDocument) => adminDocument.documentID == id);
  }
}
