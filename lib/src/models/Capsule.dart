import 'package:cloud_firestore/cloud_firestore.dart';

class Capsule {
  String _id;
  String _name;
  int _intensity;
  String _brand;

  Capsule.fromSnapshot(DocumentSnapshot parsedDocument) {
      _id = parsedDocument.documentID;
      _name = parsedDocument.data['name'];
      _intensity = parsedDocument.data['intensity'];
      _brand = parsedDocument.data['brand'];
  }

  String get id => _id;

  String get name => _name;

  int get intensity => _intensity;

  String get brand => _brand;
}