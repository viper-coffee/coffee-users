import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:coffe_users/src/models/Capsule.dart';

class Purchase {
  final String id;
  final DateTime date;
  final double price;
  final int quantity;
  final DocumentReference reference;
  List<Capsule> capsules;

  Purchase.fromMap(Map<String, dynamic> map, {this.reference})
      : assert(map['date'] != null),
        assert(map['price'] != null),
        id = reference.documentID,
        date = map['date'],
        price = map['price'].toDouble(),
        quantity = map['quantity'];

  Purchase.fromSnapshot(DocumentSnapshot snapshot)
      : this.fromMap(snapshot.data, reference: snapshot.reference);
}
