class MinimalUserInfo {
  String _email;
  bool _isAdmin;

  static const _EMAIL_FIELD_NAME = "email";
  static const _IS_ADMIN_FIELD_NAME = "isAdmin";

  MinimalUserInfo(String email, bool isAdmin) {
    _email = email;
    _isAdmin = isAdmin;
  }

  MinimalUserInfo.fromJson(Map<String, dynamic> parsedJson) {
    _email = parsedJson[_EMAIL_FIELD_NAME];
    _isAdmin = parsedJson[_IS_ADMIN_FIELD_NAME] == 1;
  }

  Map<String, dynamic> toJson() => {
    _EMAIL_FIELD_NAME: email,
    _IS_ADMIN_FIELD_NAME: isAdmin? 1 : 0,
  };

  String get email => _email;

  bool get isAdmin => _isAdmin;
}