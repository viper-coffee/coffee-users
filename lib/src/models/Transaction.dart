import '../models/Payment.dart';
import '../models/Purchase.dart';

class Transaction {
  DateTime _date;
  double _sum;
  int _purchaseCapsulesQuantity;

  Transaction.fromPayment(Payment payment) {
    _date = payment.date;
    _sum = -1 * payment.amount;
  }

  Transaction.fromPurchase(Purchase purchase) {
    _date = purchase.date;
    _sum = purchase.price;
    _purchaseCapsulesQuantity = purchase.quantity;
  }

  double get sum => _sum;

  DateTime get date => _date;

  int get purchaseCapsulesQuantity => _purchaseCapsulesQuantity;
}