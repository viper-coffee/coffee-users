import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:coffe_users/src/models/Payment.dart';
import 'package:coffe_users/src/models/Purchase.dart';

class Customer {
  final String _id;
  final String displayName;

  final String profilePictureUrl;
  final DocumentReference reference;
  List<Purchase> purchases;
  List<Payment> payments;

  static const DISPLAY_NAME_FIELD_NAME = "display_name";
  static const PICTURE_URL_FIELD_NAME = "picture_url";
  static const EMAIL_FIELD_NAME = "email";

  int totalCapsulesPurchased() {
    int startValue = 0;
    return purchases != null
        ? purchases.fold(startValue, (prevSum, next) => prevSum + next.quantity)
        : 0;
  }

  double totalPurchasesCost() {
    double startValue = 0;
    return purchases != null
        ? purchases.fold(startValue, (prevSum, next) => prevSum + next.price)
        : 0;
  }

  Customer.fromMap(Map<String, dynamic> map, {this.reference})
      : assert(map[DISPLAY_NAME_FIELD_NAME] != null),
        assert(map[PICTURE_URL_FIELD_NAME] != null),
        displayName = map[DISPLAY_NAME_FIELD_NAME],
        profilePictureUrl = map[PICTURE_URL_FIELD_NAME],
        _id = reference.documentID;

  Customer.fromSnapshot(DocumentSnapshot snapshot)
      : this.fromMap(snapshot.data, reference: snapshot.reference);

  String get id => _id;
}
