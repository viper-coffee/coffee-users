import 'package:cloud_firestore/cloud_firestore.dart';

class Payment {
  final String id;
  final DateTime date;
  final double amount;
  final DocumentReference reference;

  Payment.fromMap(Map<String, dynamic> map, {this.reference})
      : assert(map['date'] != null),
        assert(map['amount'] != null),
        id = reference.documentID,
        date = map['date'],
        amount = map['amount'].toDouble();

  Payment.fromSnapshot(DocumentSnapshot snapshot)
      : this.fromMap(snapshot.data, reference: snapshot.reference);
}
