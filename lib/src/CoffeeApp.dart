import 'package:coffe_users/src/blocs/LoginBloc.dart';
import 'package:coffe_users/src/ui/screens/LogInSplashScreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

class CoffeeApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    projectLoginBloc.fetchConnectedUserFromDB();

    return MaterialApp(
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        Locale("en", "US"),
        Locale("he", "IL"),
      ],
      locale: Locale("he", "IL"),
      home: LogInSplashScreen(),
      title: "Viper Coffee",
    );
  }
}
