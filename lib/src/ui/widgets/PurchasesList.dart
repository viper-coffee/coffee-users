import 'package:coffe_users/src/models/Customer.dart';
import 'package:coffe_users/src/models/Transaction.dart';
import 'package:flutter/material.dart';
import './PurchasesListItem.dart';

class PurchasesList extends StatefulWidget {
  PurchasesList(this.connectedCustomer);

  final Customer connectedCustomer;

  @override
  State<StatefulWidget> createState() =>
      _PurchasesListState(this.connectedCustomer);
}

class _PurchasesListState extends State<PurchasesList>
    with SingleTickerProviderStateMixin {
  _PurchasesListState(this.connectedCustomer);

  final Customer connectedCustomer;

  static const _ALL_TRANSACTIONS_FILTER_LABEL = "הכל";
  static const _ONLY_PURCHASES_FILTER_LABEL = "רכישות";
  static const _ONLY_PAYMENTS_FILTER_LABEL = "תשלומים";

  static const List<Tab> _FILTER_TRANSACTIONS_TABS = <Tab>[
    Tab(text: _ALL_TRANSACTIONS_FILTER_LABEL),
    Tab(text: _ONLY_PURCHASES_FILTER_LABEL),
    Tab(text: _ONLY_PAYMENTS_FILTER_LABEL),
  ];

  @override
  Widget build(BuildContext context) {
    List<Transaction> allTransactions = [];

    connectedCustomer.purchases?.forEach(
            (purchase) => allTransactions.add(Transaction.fromPurchase(purchase)));
    connectedCustomer.payments?.forEach(
            (payment) => allTransactions.add(Transaction.fromPayment(payment)));

    allTransactions.sort((a, b) =>
    b.date.millisecondsSinceEpoch - a.date.millisecondsSinceEpoch);

    return DefaultTabController(
      length: _FILTER_TRANSACTIONS_TABS.length,
      child: Column(
        children: <Widget>[
          Container(
            color: Color(0xFF819ca9),
            child: TabBar(
              tabs: _FILTER_TRANSACTIONS_TABS,
            ),
          ),
          Container(
            height: MediaQuery.of(context).size.height / 5 * 3 -
                (1 * MediaQuery.of(context).size.height / 26),
            child: TabBarView(
              children: <Widget>[
                _buildList(allTransactions),
                _buildList(allTransactions
                    .where((transaction) =>
                        transaction.purchaseCapsulesQuantity != null)
                    .toList()),
                _buildList(allTransactions
                    .where((transaction) =>
                        transaction.purchaseCapsulesQuantity == null)
                    .toList())
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildList(List<Transaction> transactions) {
    return Container(
      height: MediaQuery.of(context).size.height / 5 * 3 -
          (1 * MediaQuery.of(context).size.height / 43),
      child: ListView.builder(itemBuilder: (BuildContext context, int i) {
        if (i ~/ 2 >= transactions.length) {
          return null;
        } else if (i.isOdd) {
          return Divider();
        }

        final int index = i ~/ 2;

        return index < transactions.length
            ? PurchasesListItem(transactions[index])
            : null;
      }),
    );
  }
}
