import 'package:flutter/material.dart';
import '../../models/Transaction.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../../utils/dateTimeStringFormatter.dart';

class PurchasesListItem extends StatelessWidget {
  PurchasesListItem(this.info);

  final Transaction info;

  @override
  Widget build(BuildContext context) {
    Color tileColor = info.sum > 0 ? Colors.black54 : Colors.green[400];
    return ListTile(
      leading: Icon(
          FontAwesomeIcons.coffee,
          color: tileColor,
        ),
      title: Text(
        "₪" + (info.sum.abs()).toStringAsFixed(2),
        style: TextStyle(color: tileColor, fontFamily: "Cardo"),
      ),
      subtitle: info.purchaseCapsulesQuantity != null ? Text(info.purchaseCapsulesQuantity.toString() + " קפסולות נקנו",) : null,
      trailing: Padding(
        padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 30.0),
        child: Text(
          DateTimeStringFormatter().getTimeString(info.date),
          style: TextStyle(fontSize: 9.0),
        ),
      ),
    );
  }
}
