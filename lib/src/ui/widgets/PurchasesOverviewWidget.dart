import 'package:coffe_users/src/models/Customer.dart';
import 'package:coffe_users/src/models/Transaction.dart';
import 'package:flutter/material.dart';
import '../../blocs/CustomerBloc.dart';

class PurchasesOverviewWidget extends StatefulWidget {
  PurchasesOverviewWidget(this.connectedCustomer);

  final Customer connectedCustomer;

  @override
  State<StatefulWidget> createState() =>
      _PurchasesOverviewState(this.connectedCustomer);
}

class _PurchasesOverviewState extends State<PurchasesOverviewWidget>
    with SingleTickerProviderStateMixin {
  _PurchasesOverviewState(this.connectedCustomer) {
    projectCustomerBloc.fetchCustomerData();
  }

  final Customer connectedCustomer;

  @override
  Widget build(BuildContext context) {
    List<Transaction> allTransactions = [];

    connectedCustomer.purchases?.forEach(
            (purchase) => allTransactions.add(Transaction.fromPurchase(purchase)));
    connectedCustomer.payments?.forEach(
            (payment) => allTransactions.add(Transaction.fromPayment(payment)));

    allTransactions.sort((a, b) =>
    b.date.millisecondsSinceEpoch - a.date.millisecondsSinceEpoch);

    return _buildOverviewWidget(allTransactions);
  }

  Widget _buildOverviewWidget(List<Transaction> transactions) {
    double debt = _calculateDebt(transactions);
    return Container(
      height: MediaQuery.of(context).size.height / 4,
      color: Color(0xFF546e7a),
      alignment: Alignment.center,
      child: Column(children: [
        Padding(
          padding: EdgeInsets.fromLTRB(0.0, 50.0, 0.0, 0.0),
          child: Text("סך הכל חייב:",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 27.0,
                  fontStyle: FontStyle.italic),
              overflow: TextOverflow.ellipsis,
              textAlign: TextAlign.center),
        ),
        Text(
          debt.toStringAsFixed(2) + " ₪ ",
          style: TextStyle(
              color: debt > 0 ? Colors.red[400] : Colors.green[400],
              fontSize: 30.0,
              fontStyle: FontStyle.italic),
          textAlign: TextAlign.center,
          overflow: TextOverflow.ellipsis,
        ),
      ]),
    );
  }

  double _calculateDebt(List<Transaction> transactions) {
    double sum = 0;

    transactions.forEach((Transaction transaction) {
      sum += transaction.sum;
    });

    return sum;
  }
}
