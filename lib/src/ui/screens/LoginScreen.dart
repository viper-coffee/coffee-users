import 'dart:async';

import 'package:coffe_users/src/ui/screens/LogInSplashScreen.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../../blocs/LoginBloc.dart';

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  _LoginScreenState() {
    this.didStartLoginSubscription =
        projectLoginBloc.didStartLogin.listen((didStartLogin) {
      if (didStartLogin == true) _navigateToSplashScreen();
    });

    this.didStartLoginSubscription.onError((dynamic error) {
      final snackBar = SnackBar(content: Text('לא התחבר בהצלחה'));

      Scaffold.of(context).showSnackBar(snackBar);
    });
  }

  StreamSubscription<bool> didStartLoginSubscription;

  _signIn() async {
    await projectLoginBloc.signIn();
  }

  _navigateToSplashScreen() {
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => LogInSplashScreen()));
  }

  Widget _buildLoginScreenBody() {
    return Stack(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/login-background.jpeg"),
              fit: BoxFit.cover,
            ),
          ),
        ),
        Center(
          child: Container(
            width: 150.0,
            height: 70.0,
            child: FloatingActionButton(
              onPressed: _signIn,
              backgroundColor: Colors.brown[400],
              child: Icon(FontAwesomeIcons.google),
            ),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    projectLoginBloc.signOut();
    return Scaffold(
      body: _buildLoginScreenBody(),
    );
  }

  @override
  void dispose() {
    this.didStartLoginSubscription.cancel();
    super.dispose();
  }
}
