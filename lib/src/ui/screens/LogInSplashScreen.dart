import 'dart:async';
import 'package:coffe_users/src/blocs/LoginBloc.dart';
import 'package:coffe_users/src/models/MinimalUserInfo.dart';
import 'package:coffe_users/src/ui/screens/LoginScreen.dart';
import 'package:coffe_users/src/ui/screens/UserScreen.dart';
import 'package:flutter/material.dart';

class LogInSplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LogInSplashScreenState();
}

class _LogInSplashScreenState extends State<LogInSplashScreen> {
  _LogInSplashScreenState() {
    this.connectedUserSubscription =
        projectLoginBloc.connectedUser.listen((connectedUser) {
      if (connectedUser != null) {
        _navigateToScreen(UserScreen());
      } else {
        projectLoginBloc.didStartLogin.listen((didStartLogin) {
          if (!didStartLogin) _navigateToScreen(LoginScreen());
        });
      }
    });
  }

  StreamSubscription<MinimalUserInfo> connectedUserSubscription;

  _navigateToScreen(Widget screen) {
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => screen));
  }

  Widget _buildSplashScreenBody() {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/login-loading.gif"),
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildSplashScreenBody(),
    );
  }

  @override
  void dispose() {
    this.connectedUserSubscription.cancel();
    super.dispose();
  }
}
