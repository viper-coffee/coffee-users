import 'package:cached_network_image/cached_network_image.dart';
import 'package:coffe_users/src/blocs/CustomerBloc.dart';
import 'package:coffe_users/src/models/Customer.dart';
import 'package:coffe_users/src/ui/screens/LoginScreen.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../../blocs/LoginBloc.dart';
import 'package:flutter/material.dart';

class ProfileScreen extends StatelessWidget {
  ProfileScreen(this.connectedCustomer);

  final Customer connectedCustomer;

  static final _LOG_OUT_COLOR = Colors.red[400];
  static const _TEXT_FONT_SIZE = 35.0;
  static const _PROFILE_PICTURE_HERO_TAG = "profile-picture";

  _signOut(BuildContext context) async {
    await projectLoginBloc.signOut();
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => LoginScreen()),
        (Route<dynamic> route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF29434e),
        title: Text(
          "פרופיל",
          style: TextStyle(fontStyle: FontStyle.italic),
        ),
      ),
      body: Container(
        color: Color(0xFF546e7a),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            FutureBuilder<dynamic>(
                future: projectCustomerBloc
                    .getImageDownloadUrl(connectedCustomer.profilePictureUrl),
                builder:
                    (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.waiting:
                      return CircularProgressIndicator();
                    case ConnectionState.done:
                      return Container(
                        padding: EdgeInsets.symmetric(
                            horizontal: 20.0, vertical: 5.0),
                        child: Hero(
                          tag: _PROFILE_PICTURE_HERO_TAG,
                          child: CircleAvatar(
                            backgroundImage:
                                CachedNetworkImageProvider(snapshot.data),
                            radius: 100,
                          ),
                        ),
                      );
                    default:
                      return CircularProgressIndicator();
                  }
                }),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Text(
                  "${connectedCustomer.displayName}",
                  style: TextStyle(fontSize: _TEXT_FONT_SIZE, color: Colors.white, fontStyle: FontStyle.italic),
                )
              ],
            ),
            FlatButton(
              onPressed: () => _signOut(context),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(FontAwesomeIcons.signOutAlt, color: _LOG_OUT_COLOR),
                  Padding(
                    padding: EdgeInsets.only(right: 20.0),
                    child: Text(
                      "התנתק",
                      style: TextStyle(
                          fontSize: _TEXT_FONT_SIZE / 2, color: _LOG_OUT_COLOR),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
