import 'package:coffe_users/src/blocs/CustomerBloc.dart';
import 'package:coffe_users/src/models/Customer.dart';
import 'package:coffe_users/src/models/Transaction.dart';
import 'package:coffe_users/src/ui/screens/ProfileScreen.dart';
import 'package:flutter/material.dart';
import '../widgets/PurchasesOverviewWidget.dart';
import '../widgets/PurchasesList.dart';
import 'package:cached_network_image/cached_network_image.dart';

class UserScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _UserScreenState();
}

class _UserScreenState extends State<UserScreen> {
  _UserScreenState() {
    projectCustomerBloc.fetchCustomerData();
  }

  static const _PROFILE_PICTURE_HERO_TAG = "profile-picture";

  Widget _buildScreenBody(Customer connectedCustomer) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        PurchasesOverviewWidget(connectedCustomer),
        PurchasesList(connectedCustomer),
      ],
    );
  }

  Widget buildScreen(Customer connectedCustomer) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF29434e),
        title: Text(
          "מעקב אחר רכישות",
          style: TextStyle(fontStyle: FontStyle.italic),
        ),
        actions: <Widget>[
          FutureBuilder<dynamic>(
              future: projectCustomerBloc
                  .getImageDownloadUrl(connectedCustomer.profilePictureUrl),
              builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                switch (snapshot.connectionState) {
                  case ConnectionState.waiting:
                    return CircularProgressIndicator();
                  case ConnectionState.done:
                    return Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
                      child: InkWell(
                        onTap: () => Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    ProfileScreen(connectedCustomer))),
                        child: Hero(
                          tag: _PROFILE_PICTURE_HERO_TAG,
                          child: CircleAvatar(
                            backgroundImage:
                                CachedNetworkImageProvider(snapshot.data),
                            radius: 25,
                          ),
                        ),
                      ),
                    );
                  default:
                    return CircularProgressIndicator();
                }
              }),
        ],
      ),
      body: _buildScreenBody(connectedCustomer),
    );
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: projectCustomerBloc.connectedCustomer,
        builder: (BuildContext context, AsyncSnapshot<Customer> snapshot) {
          if (!snapshot.hasData) {
            return Center(child: CircularProgressIndicator());
          }

          return buildScreen(snapshot.data);
        });
  }
}
