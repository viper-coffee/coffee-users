import 'package:coffe_users/src/models/MinimalUserInfo.dart';
import 'package:coffe_users/src/resources/AdminRepository.dart';
import 'package:coffe_users/src/resources/AppUserRepository.dart';
import 'package:coffe_users/src/resources/CustomersRepository.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:rxdart/rxdart.dart';

class LoginBloc {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  final GoogleSignIn _googleSignIn = GoogleSignIn();
  final _appUsersRepository = AppUserRepository();
  final _customersRepository = CustomersRepository();
  final _adminsRepository = AdminRepository();
  final _didStartLoginFetcher = BehaviorSubject<bool>();
  final _connectedUserFetcher = BehaviorSubject<MinimalUserInfo>();

  Observable<bool> get didStartLogin => _didStartLoginFetcher.stream;

  Observable<MinimalUserInfo> get connectedUser => _connectedUserFetcher.stream;

  signIn() async {
    final GoogleSignInAccount googleUser = await _googleSignIn.signIn();

    final GoogleSignInAuthentication googleAuth =
        await googleUser.authentication;

    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );

    _didStartLoginFetcher.sink.add(true);

    final FirebaseUser generatedUser =
        await _firebaseAuth.signInWithCredential(credential);

    bool isAdmin = await _adminsRepository.doesAdminExists(generatedUser.email);

    if (!isAdmin) {
      bool isRegistered =
          await _customersRepository.doesCustomerExists(generatedUser.email);
      if (!isRegistered) {
        await _customersRepository.registerUser(generatedUser.email,
            generatedUser.displayName, generatedUser.photoUrl);
      }
    }

    await _appUsersRepository
        .addUserToDB(MinimalUserInfo(generatedUser.email, isAdmin));

    MinimalUserInfo connectedUser =
        await _appUsersRepository.getMinimalUserInfoFromDB();
    _connectedUserFetcher.sink.add(connectedUser);
  }

  fetchConnectedUserFromDB() async{
    MinimalUserInfo connectedUserFromDB = await _appUsersRepository.getMinimalUserInfoFromDB();
    _connectedUserFetcher.sink.add(connectedUserFromDB);
    _didStartLoginFetcher.sink.add(false);
  }

  Future<void> signOut() async {
    await _googleSignIn.signOut();
    _didStartLoginFetcher.sink.add(false);
    _connectedUserFetcher.sink.add(null);
    return _appUsersRepository.removeUserFromDB();
  }

  dispose() {
    _didStartLoginFetcher.close();
    _connectedUserFetcher.close();
  }
}

final projectLoginBloc = LoginBloc();
