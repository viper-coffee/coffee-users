import 'package:coffe_users/src/models/MinimalUserInfo.dart';
import 'package:coffe_users/src/models/Customer.dart';
import 'package:coffe_users/src/models/Payment.dart';
import 'package:coffe_users/src/resources/AppUserRepository.dart';
import '../models/Purchase.dart';
import 'package:rxdart/rxdart.dart';
import '../resources/CustomersRepository.dart';

class CustomerBloc {
  final _usersRepository = CustomersRepository();
  final _appUserRepository = AppUserRepository();
  final _connectedCustomerFetcher = BehaviorSubject<Customer>();

  Observable<Customer> get connectedCustomer => _connectedCustomerFetcher.stream;

  fetchCustomerData() async {
    if (!_connectedCustomerFetcher.hasValue) {
      MinimalUserInfo appUser = await _appUserRepository.getMinimalUserInfoFromDB();
      String uId = appUser.email;

      _usersRepository.getCustomer(uId).listen((customerDocument) {
        Customer appCustomer = Customer.fromSnapshot(customerDocument);

        _usersRepository.getAllUserPurchases(uId).listen((
            purchaseListSnapshot) {
          appCustomer.purchases =
              purchaseListSnapshot.documents.map((purchaseSnapshot) =>
                  Purchase.fromSnapshot(purchaseSnapshot)).toList();
          _connectedCustomerFetcher.sink.add(appCustomer);
        });

        _usersRepository.getAllUserPayments(uId).listen((paymentListSnapshot) {
          appCustomer.payments =
              paymentListSnapshot.documents.map((paymentSnapshot) =>
                  Payment.fromSnapshot(paymentSnapshot)).toList();
          _connectedCustomerFetcher.sink.add(appCustomer);
        });
      });
    }
  }

  Future<dynamic> getImageDownloadUrl(String firebaseStorageUrl) async {
    return _usersRepository.getImageDownloadUrl(firebaseStorageUrl);
  }

  dispose() {
    _connectedCustomerFetcher.close();
  }
}

final projectCustomerBloc  = CustomerBloc();